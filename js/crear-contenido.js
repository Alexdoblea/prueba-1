$(function() {
    //Inserta imagenes con su nombre desde la galeria al modal
    $(".imagenesGaleria").click(function(){
        let ruta = $(this).attr("src");
        $("#imagenModalGaleria").attr("src", ruta);
        let nombre = $(this).attr("alt");
        $("#textomodal").text(nombre)
    });

    //Insertar info dentro de modal para tarjetas
    $(".tituloTARJETA").click(function(){
        let texto = $(this).children(".textoTarjeta").text();
        $("#textoModalT").text(texto);
    });
});