$(function() {
    //Valida el formato de rut mediante expresion regular
    jQuery.validator.addMethod("validarRUT", function validarRUT(rut) {
          if (!rut | typeof rut !== 'string') return false;
          var regexp = /^\d{7,8}-[k|K|\d]{1}$/;
          return regexp.test(rut);
      });

    //Valida el numero de telefono con el formato correcto +56 mediante expresion regular
    jQuery.validator.addMethod("validarNUMERO", function validarNUMERO(numero) {
        if (!numero | typeof numero !== 'string') return true;
        var regexp = /^[+569]{4}-\d{4}-\d{4}$/
        return regexp.test(numero);
    });

    //Valida el formulario
    $("#miMODALFORM").validate({
    rules: {
        miCORREO:{required:true, email:true},
        miRUT: {required:true, validarRUT:true},
        miNOMBRE: {required: true, minlength:5, maxlength:50},
        miFECHA:{required:true},
        miNUMERO:{validarNUMERO:true}
    },
    messages: {
        miCORREO:{required:"Este campo es obligatorio", email:"Ingrese un correo valido"},
        miRUT: {required:"Este campo es obligatorio",
        validarRUT:"El formato es incorrecto ej:12345678-9"},
        miNOMBRE: {required: "Este campo es obligatorio", minlength:"Minimo 5 carácteres", maxlength:"Maximo 50 carácteres"},
        miFECHA:{required:"Este campo es obligatorio"},
        miNUMERO:{validarNUMERO:"El formato es incorrecto ej:+569-1234-5678"}
    }
    });
});